import React from 'react'

const ItemShoe = (props) => {
    const { item, hanldChoose } = props
    console.log({ item });
    return (
        <div className=''>
            <img className='w-4/5' src={item.image} alt="" />
            <div className="cart__body  pl-3  h-20">
                <h2 className='pt-2 text-lime-400 text-xl h-14' >{item.name}</h2>
                <p className='pt-1 font-bold'>{item.price} $</p>
            </div>
            <div className='cart__footer pl-2 pt-2 pb-2'>
                <button onClick={() => hanldChoose(item)} class="bg-transparent block hover:bg-slate-500 text-neutral-500 font-semibold hover:text-white py-2 px-4 border hover:border-transparent rounded " data-modal-target="defaultModal" data-modal-toggle="defaultModal" >
                    Thông tin chi tiết
                </button>
            </div>
        </div >
    )
}

export default ItemShoe