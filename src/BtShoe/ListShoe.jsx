import React from 'react'
import ItemShoe from './ItemShoe';

const ListShoe = (props) => {
    const { data, hanldChoose } = props;
    return (
        <div className='grid grid-cols-4 gap-5 pt-20 '>
            {
                data.map((item) =>
                (
                    <div key={item.id} className="shadow-inner bg-gray-100 rounded-lg ">
                        <ItemShoe item={item} hanldChoose={hanldChoose} />
                    </div>)

                )
            }
        </div>
    )
}

export default ListShoe