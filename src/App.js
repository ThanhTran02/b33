import BtShoe from './BtShoe/BtShoe';

function App() {
  return (
    <div className="App">
      <BtShoe />
    </div>
  );
}

export default App;
